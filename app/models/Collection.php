<?php

use Magniloquent\Magniloquent\Magniloquent;

class Collection extends Magniloquent {

  /**
   * Factory for tests.
   * 
   * @var array
   */
  public static $factory = array(
    'name'      => 'string',
    'category'  => 'factory|Category',
    'is_public' => 'true'
  );

  /**
   * Validation rules.
   *
   * @var array
   */
  public static $rules = array(
    'save' => array(
      'name'      => 'required|alpha_dash|between:3,20',
      'category'  => 'integer',
      'is_public' => 'required'
    ),
    'create' => array(
      'name'      => 'required|alpha_dash|between:3,20',
      'category'  => 'integer',
      'is_public' => 'required'
    ),
    'update' => array()
  );
	
  /**
   * The database used by the model.
   * 
   * @var string
   */
  protected $table = 'collections';

  /**
   * Get the name of the collection.
   * 
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get the is visibility status of the collection.
   * 
   * @return boolean
   */
  public function isPublic()
  {
    return (bool) $this->is_public;
  }

  /**
   * Get the category of the collection.
   * 
   * @return int
   */
  public function category()
  {
    return $this->hasOne('Category');
  }

  /**
   * Relationship with User.
   * 
   * @return mixed
   */
  public function users()
  {
    return $this->belongsToMany('User', 'collection_user', 'collection_id', 'user_id')->withTimestamps();
  }

  public function media()
  {
    return $this->belongsToMany('Medium', 'collection_medium', 'collection_id', 'medium_id')->withTimestamps();
  }

}
