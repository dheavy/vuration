@extends('layouts.master')

@section('title')
@parent
:: Login
@stop

@section('content')
{{ Form::open(array('route' => 'session.store')) }}

<div class="page-header">
  <h2>Log in</h2>
</div>

@if (Session::has('login_errors'))
  <h5 class="error">Email or password incorrect.</h5>
@endif

  <!-- Email -->
  <div class="control-group {{{ $errors->has('email') ? 'error' : '' }}}">
    {{ Form::label('email', 'E-Mail', array('class' => 'control-label')) }}

    <div class="controls">
      {{ Form::text('email') }}
      {{ $errors->first('email') }}
    </div>
  </div>

  <!-- Password -->
  <div class="control-group {{{ $errors->has('password') ? 'error' : '' }}}">
    {{ Form::label('password', 'Password', array('class' => 'control-label')) }}

    <div class="controls">
    {{ Form::password('password') }}
    {{ $errors->first('password') }}
    </div>
  </div>

  <!-- Password confirmation -->
  <div class="control-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
    {{ Form::label('password_confirmation', 'Confirm password', array('class' => 'control-label')) }}

    <div class="controls">
    {{ Form::password('password_confirmation') }}
    </div>
  </div>

  <!-- Login button -->
  <div class="control-group" style="margin-top:15px">
    <div class="controls">
      {{ Form::submit('Login', array('class' => 'btn btn-primary')) }}
    </div>
  </div>

  {{ Form::close() }}
@stop