<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddVideoToTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('types')->insert(
			array(
				'name' => 'video',
				'created_at' => 'NOW()',
				'updated_at' => 'NOW()'
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('types')->where('name', '=', 'video')->delete();
	}

}