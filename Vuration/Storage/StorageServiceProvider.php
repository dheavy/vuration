<?php

namespace Vuration\Storage;

use Illuminate\Support\ServiceProvider;

class StorageServiceProvider extends ServiceProvider {

  public function register()
  {
    $this->app->bind(
      'Vuration\Storage\User\UsersRepository',
      'Vuration\Storage\User\EloquentUserRepository'
    );
  }

}