<?php

// Register
Route::get('register', array(
  'uses' => 'RegisterController@index',
  'as'   => 'register.index'
));

Route::post('register', array(
  'uses' => 'RegisterController@store',
  'as'   => 'register.store'
));

// Authentication
Route::get('login', array(
  'uses' => 'SessionController@create',
  'as'   => 'session.create'
));
Route::post('login', array(
  'uses' => 'SessionController@store',
  'as'   => 'session.store'
));
Route::get('logout', array(
  'uses' => 'SessionController@destroy',
  'as'   => 'session.destroy'
));

// Public
Route::get('/', array(
  'uses' => 'HomeController@index',
  'as'   => 'home.index'
));

// Resources
Route::resource('users', 'UsersController');