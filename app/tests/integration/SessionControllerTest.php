<?php

class SessionControllerTest extends TestCase {

  public function testCreate()
  {
    $this->get('login');
    $this->assertResponseOk();

    // Temporary fix so it doesn't crash my machine.
    ini_set('memory_limit', '64M');
  }

  public function testStoreFails()
  {
    Auth::shouldReceive('attempt')->andReturn(false);

    $this->post('login');
    $this->assertRedirectedToRoute('session.create');
    $this->assertSessionHas('flash');
  }

  public function testStoreSuccess()
  {
    Auth::shouldReceive('attempt')->andReturn(true);
    $this->post('login');
    $this->assertRedirectedToRoute('home.index');
  }

}