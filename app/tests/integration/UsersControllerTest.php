<?php

use \Mockery as m;

class UsersControllerTest extends TestCase {

  public function setUp()
  {
    parent::setUp();
    $this->mock = $this->mock('Vuration\Storage\User\UsersRepository');
  }

  public function tearDown()
  {
    parent::tearDown();
    m::close();
  }

  public function mock($class)
  {
    $mock = m::mock($class);
    $this->app->instance($class, $mock);
    return $mock;
  }

  public function testIndex()
  {
    $this->mock
         ->shouldReceive('all')
         ->once();

    $this->get('users');
    $this->assertResponseOk();
    $this->assertViewHas('users');
  }

  /**
   * Save failed: user is redirected back to
   * create method with errors.
   */
  public function testStoreFails()
  {
    $this->mock
         ->shouldReceive('create')
         ->once()
         ->andReturn(m::mock(array(
            'isSaved' =>false,
            'errors'  => array()
          )));


    $this->post('users');

    $this->assertRedirectedToRoute('users.create');
    $this->assertSessionHasErrors();
  }

  /**
   * Save succeeded: user redirected to users/index
   * with a flash message.
   */
  public function testStoreSuccess()
  {
    $this->mock
         ->shouldReceive('create')
         ->once()
         ->andReturn(m::mock(array(
            'isSaved' => true
          )));

    $this->post('users');

    $this->assertRedirectedToRoute('users.index');
    $this->assertSessionHas('flash');
  }

  public function testShow()
  {
    $this->mock
         ->shouldReceive('find')
         ->once()
         ->with(1);

    $this->get('users/1');
    $this->assertResponseOk();
  }

  public function testEdit()
  {
    $user = m::self();
    $user->id = 1;

    $this->mock
         ->shouldReceive('find')
         ->once()
         ->with(1)
         ->andReturn($user);

    $this->get('users/1/edit');

    $this->assertResponseOk();
  }

  public function testUpdateFails()
  {
    $this->mock
         ->shouldReceive('update')
         ->once()
         ->with(1)
         ->andReturn(m::mock(array(
            'isSaved' => false,
            'errors'  => array()
          )));

    $this->put('users/1');

    $this->assertRedirectedToRoute('users.edit', 1);
    $this->assertSessionHasErrors();
  }

  public function testUpdateSuccess()
  {
    $this->mock
         ->shouldReceive('update')
         ->once()
         ->with(1)
         ->andReturn(m::mock(array(
            'isSaved' => true
          )));

    $this->put('users/1');

    $this->assertRedirectedToRoute('users.show', 1);
    $this->assertSessionHas('flash');
  }

}