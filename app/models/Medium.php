<?php

use Magniloquent\Magniloquent\Magniloquent;

class Medium extends Magniloquent {

  /**
   * The database table used by the model.
   * 
   * @var string
   */
  protected $table = 'media';

  /**
   * Factory for tests.
   * 
   * @var array
   */
  public static $factory = array(
    'type'        => 'factory|Type',
    'name'        => 'string',
    'description' => 'string',
    'src_name'    => 'string',
    'src_url'     => 'string',
    'src_code'    => 'string'
  );

  /**
   * Validation rules.
   *
   * @var array
   */
  public static $rules = array(
    'save' => array(
      'name' => 'required|between:3,30',
      'src_name' => 'required|max:50',
      'src_code' => 'required'
    ),
    'create' => array(
      'name' => 'required|between:3,30',
    ),
    'update' => array()
  );

  /**
   * Get the name of the medium.
   * 
   * @return string 
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get the description of the medium.
   * 
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Get the name for the medium source, usually the title of the original page.
   * 
   * @return string 
   */
  public function getSrcName()
  {
    return $this->src_name;
  }

  /**
   * Get the URL for the medium source: the original page URL without the params.
   * 
   * @return string
   */
  public function getSrcUrl()
  {
    return $this->src_url;
  }

  /**
   * Get the source code for the medium, usually the embed code from the platform, or scrapped from the page directly.
   * 
   * @return string
   */
  public function getSrcCode()
  {
    return $this->src_code;
  }

  /**
   * Get the type of the medium.
   * 
   * @return mixed 
   */
  public function type()
  {
    return $this->hasOne('Type');
  }

  /**
   * Relationship with Collection.
   * 
   * @return mixed 
   */
  public function collections()
  {
    return $this->belongsToMany('Collection', 'collection_medium', 'medium_id', 'collection_id')->withTimestamps();
  }

}