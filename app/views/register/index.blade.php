@extends('layouts.master')

@section('title')
@parent
:: Register
@stop

@section('content')
<div class="page-header">
  <h2>Join Vuration</h2>
</div>

  <!-- Username -->
  <div class="control-group {{{ $errors->has('username') ? 'error' : '' }}}">
    {{ Form::label('username', 'Username', array('class' => 'control-label')) }}

    <div class="controls">
      {{ Form::text('username') }}
      {{ $errors->first('username') }}
    </div>
  </div>

  <!-- Email -->
  <div class="control-group {{{ $errors->has('email') ? 'error' : '' }}}">
    {{ Form::label('email', 'E-Mail', array('class' => 'control-label')) }}

    <div class="controls">
      {{ Form::text('email') }}
      {{ $errors->first('email') }}
    </div>
  </div>

  <!-- Password -->
  <div class="control-group {{{ $errors->has('password') ? 'error' : '' }}}">
    {{ Form::label('password', 'Password', array('class' => 'control-label')) }}

    <div class="controls">
    {{ Form::password('password') }}
    {{ $errors->first('password') }}
    </div>
  </div>

  <!-- Password confirmation -->
  <div class="control-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
    {{ Form::label('password_confirmation', 'Confirm password', array('class' => 'control-label')) }}

    <div class="controls">
    {{ Form::password('password_confirmation') }}
    </div>
  </div>

  <!-- Login button -->
  <div class="control-group" style="margin-top:15px">
    <div class="controls">
      {{ Form::submit('Join', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
@stop