<?php

class CategoryTest extends TestCase {

  private $category;

  public function setUp()
  {
    parent::setUp();
    $this->category = $this->factory->create('Category');
  }

  public function tearDown()
  {
    parent::tearDown();
    unset($this->category);
  }

  public function testNameIsRequired()
  {
    unset($this->category->name);
    $this->assertFalse($this->category->save());

    $this->category->name = '';
    $this->assertFalse($this->category->save());
  }

  public function testNameIsValid()
  {
    $this->category->name = 'ab';
    $this->assertFalse($this->category->save());

    $this->category->name = 'abcdefghijklmnopqrstu';
    $this->assertFalse($this->category->save());

    $this->category->name = 'abc.de';
    $this->assertFalse($this->category->save());

    $this->category->name = 'abc>de';
    $this->assertFalse($this->category->save());

    $this->category->name = 'abc_de';
    $this->assertTrue($this->category->save());

    $this->category->name = 'abc-de';
    $this->assertTrue($this->category->save());

    $this->category->name = 'abc';
    $this->assertTrue($this->category->save());
  }

}