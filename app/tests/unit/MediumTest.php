<?php

class MediumTest extends TestCase {

  private $media;

  public function setUp()
  {
    parent::setUp();
    $this->medium = $this->factory->create('Medium');
  }

  public function tearDown()
  {
    parent::tearDown();
    unset($this->medium);
  }

  public function testNameIsRequired()
  {
    unset($this->medium->name);
    $this->assertFalse($this->medium->save());

    $this->medium->name = '';
    $this->assertFalse($this->medium->save());
  }

  public function testSrcNameIsRequired()
  {
    unset($this->medium->src_name);
    $this->assertFalse($this->medium->save());

    $this->medium->src_name = '';
    $this->assertFalse($this->medium->save());
  }

  public function testSrcCodeIsRequired()
  {
    unset($this->medium->src_code);
    $this->assertFalse($this->medium->save());

    $this->medium->src_code = '';
    $this->assertFalse($this->medium->save());
  }

  public function testRelationshipWithCollections()
  {
    $this->c1 = $this->factory->create('Collection');
    $this->c2 = $this->factory->create('Collection');
    $this->c3 = $this->factory->create('Collection');

    $this->medium->collections()->attach($this->c1);
    $this->medium->collections()->attach($this->c2);
    $this->assertEquals($this->medium->collections->first()->id, $this->c1->id);
    $this->assertEquals($this->medium->collections->last()->id, $this->c2->id);

    $this->c3->media()->attach($this->medium);
    $this->assertEquals($this->c3->media->first()->id, $this->medium->id);
  }

}