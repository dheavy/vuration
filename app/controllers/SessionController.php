<?php

use Vuration\Storage\User\UsersRepository as User;

class SessionController extends BaseController {

  protected $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }

  public function create()
  {
    return View::make('session.create');
  }

  public function store()
  {
    if (Auth::attempt(array(
      'email'    => Input::get('email'),
      'password' => Input::get('password')
    ))) {
      // Write full URL as http://localhost/ with trailing slash.
      // Just writing relatve '/' will return localhost URL without trailing slash
      // and will result as an error.
      return Redirect::intended('http://localhost/');
    }

    return Redirect::route('session.create')
                   ->withInput()
                   ->with('login_errors', true);
  }

  public function destroy()
  {
    Auth::logout();

    return View::make('session.destroy');
  }

}