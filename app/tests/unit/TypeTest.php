<?php

class TypeTest extends TestCase {

  private $type;

  public function setUp()
  {
    parent::setUp();
    $this->type = $this->factory->create('Type');
  }

  public function tearDown()
  {
    parent::tearDown();
    unset($this->type);
  }

  public function testNameIsRequired()
  {
    unset($this->type->name);
    $this->assertFalse($this->type->save());

    $this->type->name = '';
    $this->assertFalse($this->type->save());
  }

  public function testNameIsValid()
  {
    $this->type->name = 'ab';
    $this->assertFalse($this->type->save());

    $this->type->name = 'abcdefghijklmnopqrstu';
    $this->assertFalse($this->type->save());

    $this->type->name = 'abc.de';
    $this->assertFalse($this->type->save());

    $this->type->name = 'abc>de';
    $this->assertFalse($this->type->save());

    $this->type->name = 'abc_de';
    $this->assertTrue($this->type->save());

    $this->type->name = 'abc-de';
    $this->assertTrue($this->type->save());

    $this->type->name = 'abc';
    $this->assertTrue($this->type->save());
  }

}