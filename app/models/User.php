<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Magniloquent\Magniloquent\Magniloquent;

class User extends Magniloquent implements UserInterface, RemindableInterface {

  /**
   * Auto purge redundant attributes
   *
   * @var bool
   */
  public $autoPurgeRedundantAttributes = true;

  /**
   * Set mass-assignable members.
   * 
   * @var array
   */
  protected $fillable = array('username', 'email', 'password', 'password_confirmation');
  
  /**
   * Hide password from JSON representation of the model.
   * 
   * @var array
   */
  protected $hidden = array('password');

  /**
   * Enable soft delete in database.
   * 
   * @var boolean
   */
  protected $softDelete = true;

  /**
   * Factory for tests.
   *
   * @var array
   */
  public static $factory = array(
    'username'              => 'string',
    'email'                 => 'email',
    'password'              => 'password',
    'password_confirmation' => 'password'
  );

  /**
   * Validation rules.
   *
   * @var array
   */
  public static $rules = array(
    "save" => array(
      'username' => 'required|alpha_dash|between:3,20',
      'email'    => 'required|email',
      'password' => 'required|min:6'
    ),
    "create" => array(
      'username'              => 'unique:users',
      'email'                 => 'unique:users',
      'password'              => 'required|min:6|confirmed',
      'password_confirmation' => 'required|min:6'
    ),
    "update" => array()
  );

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  /**
   * Get the unique identifier for the user.
   *
   * @return mixed
   */
  public function getAuthIdentifier()
  {
    return $this->getKey();
  }

  /**
   * Get the password for the user.
   *
   * @return string
   */
  public function getAuthPassword()
  {
    return $this->password;
  }

  /**
   * Get the e-mail address where password reminders are sent.
   *
   * @return string
   */
  public function getReminderEmail()
  {
    return $this->email;
  }

  /**
   * Relationship with Collection.
   * 
   * @return mixed CollectionOwnership this user has.
   */
  public function collections()
  {
    return $this->belongsToMany('Collection', 'collection_user', 'user_id', 'collection_id')->withTimestamps();
  }

}