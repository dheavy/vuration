<?php

use Magniloquent\Magniloquent\Magniloquent;

/**
 * Type defines the type of a media, either video or gif or anything...
 * We'll start with video only.
 */
class Type extends Magniloquent {

  /**
   * Validation rules.
   * 
   * @var array
   */
  public static $rules = array(
    'save' => array(
      'name' => 'required|alpha_dash|between:3,20'
    ),
    'create' => array(
      'name' => 'unique:types'
    ),
    'update' => array()
  );

  /**
   * Factory for tests.
   * 
   * @var array
   */
  public static $factory = array(
    'name' => 'string'
  );

  /**
   * The database table used by the model.
   * 
   * @var string
   */
  protected $table = 'types';

  /**
   * Get the name of the type.
   * 
   * @return string 
   */
  public function getName()
  {
    return $this->name;
  }

}