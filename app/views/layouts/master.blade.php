<!DOCTYPE html>
<html>
  <head>
    <title>
      @section('title')
      Vuration
      @show
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{ HTML::style('css/bootstrap.min.css') }}
  </head>

  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{{ URL::to('') }}}">Vuration</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse">
        @if (Auth::check())
        <form class="navbar-form navbar-left" role="search">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
        @endif
        <ul class="nav navbar-nav navbar-right">
          @if (Auth::guest()) 
            <li>{{ HTML::link('login', 'Login') }}</li>
            <li>{{ HTML::link('users/create', 'Join Vuration') }}</li>
          @else
            <li>{{ HTML::link('logout', 'Logout') }}</li>
          @endif
        </ul>
      </div><!-- /.navbar-collapse -->
    </div>
  </nav>

    <!-- Container -->
    <div class="container">

      <!-- Success messages -->
      @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Success</h4>
          {{{ $message }}}
        </div>
      @endif

      <!-- Content -->
      @yield('content')
    </div>

    {{ HTML::script('js/bootstrap.min.js') }}
  </body>
</html>