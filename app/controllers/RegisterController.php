<?php

use Vuration\Storage\User\UsersRepository as User;

class RegisterController extends BaseController {

  protected $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }

  public function index()
  {
    return View::make('register.index');
  }

  public function store()
  {
    $payload = $this->user->create(Input::all());

    if ($payload->isSaved()) {
      return Redirect::route('users.index')
                     ->with('flash', 'The new user has been created');
    }

    return Redirect::route('register.index')
                   ->withInput()
                   ->withErrors($payload->errors());
  }

}