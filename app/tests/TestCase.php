<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Use method overloading to write more
	 */
	public function __call($method, $args)
	{
		if (in_array($method, ['get', 'post', 'put', 'patch', 'delete'])) {
			return $this->call($method, $args[0]);
		}

		throw new BadMethodCallException;
	}

	/**
	 * Default preparation for each setup.
	 */
	public function setUp()
	{
		parent::setUp();
		$this->prepareForTests();
	}
	
	/**
	 * Creates the application.
	 *
	 * @return Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'test';

		return require __DIR__.'/../../bootstrap/start.php';
	}

	/**
	 * Migrate test DB.
	 */
	private function prepareForTests()
	{
		Artisan::call('migrate:reset', array('--env' => 'test'));
		Artisan::call('migrate', array('--env' => 'test'));
		$this->factory = new FactoryMuff;
	}

}
