<?php

use Magniloquent\Magniloquent\Magniloquent;

/**
 * Category defines the category of a collection
 */
class Category extends Magniloquent {

  /**
   * Validation rules.
   * 
   * @var array
   */
  public static $rules = array(
    'save' => array(
      'name' => 'required|alpha_dash|between:3,20'
    ),
    'create' => array(
      'name' => 'unique:categories'
    ),
    'update' => array()
  );

  /**
   * Factory for tests.
   * 
   * @var array
   */
  public static $factory = array(
    'name' => 'string'
  );

  /**
   * The database table used by the model.
   * 
   * @var string
   */
  protected $table = 'categories';

  /**
   * Get the name of the category.
   * 
   * @return string 
   */
  public function getName()
  {
    return $this->name;
  }

}