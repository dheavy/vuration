<?php

class UserTest extends TestCase {

  private $user;

  public function setUp()
  {
    parent::setUp();
    $this->user = $this->factory->create('User');
  }

  public function tearDown()
  {
    parent::tearDown();
    unset($this->user);
  }

  public function testUsernameIsRequired()
  {
    unset($this->user->username);

    $this->assertFalse($this->user->save());

    $errors = $this->user->errors()->all();

    $this->assertCount(1, $errors);
    $this->assertEquals($errors[0], 'The username field is required.');
  }

  public function testUsernameIsValid()
  {
    $this->user->username = 'ab';
    $this->assertFalse($this->user->save());

    $this->user->username = 'abcdefghijklmnopqrstu';
    $this->assertFalse($this->user->save());

    $this->user->username = 'abc.de';
    $this->assertFalse($this->user->save());

    $this->user->username = 'abc>de';
    $this->assertFalse($this->user->save());

    $this->user->username = 'abc_de';
    $this->assertTrue($this->user->save());

    $this->user->username = 'abc-de';
    $this->assertTrue($this->user->save());

    $this->user->username = 'abc';
    $this->assertTrue($this->user->save());
  }

  public function testPasswordIsValid()
  {
    $this->user->password = 'azert';
    $this->assertFalse($this->user->save());
    
    $errors = $this->user->errors()->all();
    
    $this->assertCount(1, $errors);
    $this->assertEquals($errors[0], 'The password must be at least 6 characters.');
  }

  public function testRelationshipWithCollection()
  {
    $collection1 = $this->factory->create('Collection');
    $collection2 = $this->factory->create('Collection');

    $this->user->collections()->attach($collection1);
    $this->user->collections()->attach($collection2);

    $this->assertEquals($this->user->collections->first()->id, $collection1->id);
    $this->assertEquals($this->user->collections->last()->id, $collection2->id);
    $this->assertCount(2, $this->user->collections);
  }

}