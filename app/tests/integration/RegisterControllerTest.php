<?php

use \Mockery as m;

class RegisterControllerTest extends TestCase {

  public function setUp()
  {
    parent::setUp();
    $this->mock = $this->mock('Vuration\Storage\User\UsersRepository');
  }

  public function tearDown()
  {
    parent::tearDown();
    m::close();
  }

  public function mock($class)
  {
    $mock = m::mock($class);
    $this->app->instance($class, $mock);
    return $mock;
  }

  public function testIndex()
  {
    $this->get('register');
    $this->assertResponseOk();
  }

  public function testStoreFails()
  {
    $this->mock
         ->shouldReceive('create')
         ->once()
         ->andReturn(m::mock(array(
            'isSaved' => false,
            'errors'  => array()
          )));

    $this->post('register');
    $this->assertRedirectedToRoute('register.index');
    $this->assertSessionHasErrors();
  }

  public function testStoreSuccess()
  {
    $this->mock
         ->shouldReceive('create')
         ->once()
         ->andReturn(m::mock(array(
            'isSaved' => true
          )));

    $this->post('register');

    $this->assertRedirectedToRoute('users.index');
    $this->assertSessionHas('flash');
  }

}