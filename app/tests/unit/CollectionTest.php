<?php

class CollectionTest extends TestCase {

  private $collection;

  public function setUp()
  {
    parent::setUp();
    $this->collection = $this->factory->create('Collection');
  }

  public function tearDown()
  {
    parent::tearDown();
    unset($this->collection);
  }

  public function testCollectionNameIsRequired()
  {
    unset($this->collection->name);
    $this->assertFalse($this->collection->save());

    $errors = $this->collection->errors()->all();

    $this->assertCount(1, $errors);
    $this->assertEquals($errors[0], 'The name field is required.');
  }

  public function testIsPublicReturnsBoolean()
  {
    $this->assertTrue(is_bool($this->collection->isPublic()));
  }

  public function testRelationshipWithUser()
  {
    $user1 = $this->factory->create('User');
    $user2 = $this->factory->create('User');
    
    $user1->collections()->attach($this->collection->id);
    $user2->collections()->attach($this->collection->id);
    
    $this->assertEquals($this->collection->users->first()->id, $user1->id);
    $this->assertEquals($this->collection->users->last()->id, $user2->id);
    $this->assertCount(2, $this->collection->users);
  }

}