<?php 

namespace Vuration\Storage\User;
 
interface UsersRepository {
   
  public function all();
 
  public function find($id);
 
  public function create($input);
 
}