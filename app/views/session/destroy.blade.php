@extends('layouts.master')

@section('title')
@parent
:: Logged out
@stop

@section('content')
<h1>You have successfully logged out.</h1>
@stop